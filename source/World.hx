import nape.space.Space;
import nape.phys.Body;
import nape.phys.BodyType;
import nape.geom.Vec2;
import flixel.FlxState;
import flixel.util.FlxAngle;
import flixel.FlxSprite;
import flixel.util.FlxPoint;
import nape.callbacks.InteractionCallback;
import nape.constraint.AngleJoint;
import nape.constraint.Constraint;
import nape.constraint.DistanceJoint;
import nape.constraint.PivotJoint;
import nape.constraint.WeldJoint;
import nape.geom.Vec2;
import nape.callbacks.CbType;
import nape.callbacks.InteractionType;
import nape.callbacks.InteractionListener;
import nape.callbacks.CbEvent;
import nape.phys.Material;
import nape.phys.Interactor;
import nape.shape.Circle;
import nape.shape.Polygon;
import nape.dynamics.InteractionFilter;

class Entity {
    public var body: Body;
    public var sprite: FlxSprite;
    public var id: String;
    private var world: World;
    public var spec: String;

    public function new(id: String, specName: String, world: World) {
        this.body = new Body(this.getDefaultBodyType());
        this.id = id;
        this.world = world;
        this.spec = specName;
        this.bootstrap();
    }

    private function bootstrap() {
    }
    
    public function getDefaultBodyType(): BodyType {
        return BodyType.DYNAMIC;
    }

    public function updateSprites(world: World) {
        sprite.x = world.screenCoord(body.position.x);
        sprite.y = world.screenCoord(body.position.y);
        if (body.allowRotation) {
            sprite.angle = body.rotation * FlxAngle.TO_DEG;
        }
    }
}

class MapEntity extends Entity {
    override public function getDefaultBodyType(): BodyType {
        return BodyType.KINEMATIC;
    }
}

class JumpRockEntity extends Entity {
    override public function getDefaultBodyType(): BodyType {
        return BodyType.KINEMATIC;
    }
}

class Wheel {
    public var touchingGround: Bool = false;
    public var id: Int;
    var normalMass: Float = -1;
    public var body: Body;

    public function new(id, playerIGroup) {
        this.id = id;
        this.body = new Body();
        var wshape = new Circle(1);
        wshape.material = Material.steel();
        this.body.shapes.add(wshape);
    }

    // public function massInfinite() {
    //     this.normalMass = this.body.gravMass;
    //     this.body.gravMass = 20;
    // }

    // public function massNormal() {
    //     if (this.normalMass != -1) {
    //         this.body.gravMass = this.normalMass;
    //     }
    // }
}

class PlayerEntity extends Entity {
    public var jumping: Bool;
    public var wheels: Array<Wheel>;
    private var jumpVel: Float;
    private var wheelVel: Float;
    private var xVel: Float;
    public var midAir: Bool;
    private var direction: Int = 1;

    override private function bootstrap() {
            // body.allowRotation = false;
        midAir = false;
    }

    public function init(jumpVel: Float, wheelVel: Float, xVel: Float) {
        this.jumpVel = jumpVel;
        this.wheelVel = wheelVel;
        this.xVel = xVel;

        this.body.setShapeMaterials(Material.wood());

        wheels = new Array();
        for (i in 0...2) {
            var wheel = new Wheel(i, 2);
            world.space.bodies.add(wheel.body);
            wheels.push(wheel);
        }

        var wheel1 = wheels[0].body;
        var wheel2 = wheels[1].body;

        wheel1.position.setxy(this.body.position.x, this.body.position.y);
        var dist = this.body.bounds.width;

        wheel2.position.setxy(this.body.position.x+dist, this.body.position.y);
        var wheelsTogether:Constraint = new DistanceJoint(wheel1, wheel2, wheel1.localCOM, wheel2.localCOM,
            dist, dist);

        // var p1 = new Vec2(0, 0);
        // var p2 = new Vec2(dist, 0);
        // var list = new Array();
        // list.push(p1);
        // list.push(p2);
        // list.push(new Vec2(dist, 0.001));
        // list.push(new Vec2(0, 0.001));

        // var lineP = new Polygon(list);
        // var line = new Body(BodyType.DYNAMIC);
        // line.gravMass = 0;
        // line.shapes.add(lineP);
        // world.space.bodies.add(line);
        // line.position.setxy(player.body.position.x, player.body.position.y+10);

        var fixW1:Constraint = new PivotJoint(this.body, wheel1, new Vec2(0, this.body.bounds.height-0.5), wheel1.shapes.at(0).localCOM);
        var fixW2:Constraint = new PivotJoint(this.body, wheel2, new Vec2(dist, this.body.bounds.height-0.5), wheel2.shapes.at(0).localCOM);
        world.space.constraints.add(new AngleJoint(wheel1, wheel2, 0, 0));

        wheel2.cbTypes.add(world.FOOT);
        wheel1.cbTypes.add(world.FOOT);

        world.space.constraints.add(wheelsTogether);
        world.space.constraints.add(fixW2);
        world.space.constraints.add(fixW1);
        // world.space.constraints.add(fix);
         var touchGroundListener = new InteractionListener(CbEvent.BEGIN,
            InteractionType.COLLISION, world.SURFACE, world.FOOT, this.touchGround);
        world.space.listeners.add(touchGroundListener);
        world.space.listeners.add(new InteractionListener(CbEvent.END,
            InteractionType.COLLISION, world.SURFACE, world.FOOT, this.wheelLeavesGround));
        world.space.listeners.add(new InteractionListener(CbEvent.ONGOING,
            InteractionType.COLLISION, world.SURFACE, world.FOOT, this.stickToGround));
    }

    public function setWheelFilters(filter: InteractionFilter) {
        for (wheel in wheels) {
            wheel.body.setShapeFilters(filter);
        }
    }

    public function moveReset() {
        this.body.velocity.x = 0;
        this.body.velocity.y = 0;
        for (wheel in wheels) {
            wheel.body.angularVel = 0;
            // wheel.massNormal();
        }
    }

    public function jump() {
        if (!midAir) {
            this.body.rotation = 0;
            this.body.allowRotation = false;
            for (wheel in wheels) {
                wheel.body.velocity.y = this.jumpVel;
            }
            midAir = true;
        }
    }

    public function move(direction: Int) {
        this.direction = direction;
        var wVel = this.wheelVel*direction;
        var xVel = this.xVel*direction;
        if (midAir) {
            this.body.velocity.x = xVel;
        } else {
            for (wheel in wheels) {
                wheel.body.angularVel = wVel;
            }
        }
    }

    override public function getDefaultBodyType(): BodyType {
        return BodyType.DYNAMIC;
    }

    public function getWheelObj(body: nape.phys.Interactor): Wheel {
        for (wheel in wheels) {
            if (wheel.body == body) {
                return wheel;
            }
        }
        return null;
    }

    public function touchGround(cb:InteractionCallback): Void {
        midAir = false;
        this.body.allowRotation = true;
        var wheel = getWheelObj(cb.int2);
        wheel.touchingGround = true;

        // wheel.massNormal();
    }

    public function wheelLeavesGround(cb:InteractionCallback): Void {
        var wheel = getWheelObj(cb.int2);
        wheel.touchingGround = false;
        if (wheels[1-wheel.id].touchingGround) {
            // var dir = if (wheel.id == 0) { 1; } else { -1; };
            // this.body.applyAngularImpulse(dir * 2);
            // wheel.massInfinite();
        } else {
            this.body.angularVel = 0;
            midAir = true;
        }
    }

    public function stickToGround(cb:InteractionCallback): Void {
        var wheel = getWheelObj(cb.int2);
        if (!wheels[1-wheel.id].touchingGround) {
            var dir = if (wheel.id == 0) { 1; } else { -1; };
            this.body.applyAngularImpulse(dir * 0.1);
            // wheel.massInfinite();
        }
    }
}

class EntityFactory {
    public static function makeEntity(specName: String, id: String, world: World): Entity {
        return switch (specName) {
            case "map":
                new MapEntity(id, specName, world);
            case "player":
                new PlayerEntity(id, specName, world);
            case "jumpRock":
                new JumpRockEntity(id, specName, world);
            default:
                throw "Wrong body spec name";
        }
    }
}

class World {
    public var space: Space;
    public var entities: Map<String, Entity>;
    private var importer: EwImporter;
    private var importScale: Float;
    private var renderScale: Float;
    public var gravity: Float = 3;
    public var FOOT: CbType;
    public var SURFACE: CbType;

    public function new(importer: EwImporter, scale: Float, renderScale: Float) {
        this.importScale = scale;
        this.entities = new Map();
        this.importer = importer;

        FOOT =  new CbType();
        SURFACE = new CbType();

        var specs = importer.getBodySpecs();
        var bodies = importer.getBodies();

        this.space = new Space(new Vec2(0, this.gravity));
        for (bodyInst in bodies) {
            var entity = EntityFactory.makeEntity(bodyInst.bodyspec, bodyInst.id, this);
            var body = entity.body;
            body.position.setxy(bodyInst.pos.x*scale, bodyInst.pos.y*scale);
            // trace(bodyInst.id);
            for (shape in specs[bodyInst.bodyspec].shapes) {
                var sh = shape.adjusted(scale, bodyInst.width, bodyInst.height);
                body.shapes.add(sh.toPhysicsShape());
                // trace(shape.bounds.x, shape.bounds.y);
            }
            this.space.bodies.add(body);
            this.entities.set(bodyInst.id, entity);
        }
        this.renderScale = renderScale;
    }

    public function screenCoord(bodyCoord: Float): Float {
        return bodyCoord * this.renderScale;
    }

    public function render(target: FlxState, imagesBaseResourcePath: String) {
        var specs = this.importer.getBodySpecs();
        var instances = importer.getBodies();
        var renderScale = this.renderScale;
        for (inst in instances) {
            var body = this.entities[inst.id].body;
            var x = this.screenCoord(body.position.x);
            var y = this.screenCoord(body.position.y);
            var sprite = new FlxSprite(x, y);
            sprite.loadGraphic(imagesBaseResourcePath + specs[inst.bodyspec].imagePath);
            var factor = inst.width*this.importScale/sprite.width * renderScale;
            sprite.scale = new FlxPoint(factor, factor);
            sprite.width *= factor;
            sprite.height *= factor;
            sprite.centerOffsets();
            // target.add(sprite);
            // trace(inst.id, sprite.x, sprite.y, factor);
            // trace(sprite.width);
            this.entities[inst.id].sprite = sprite;
        }
    }

    public function updateSprites() {
        for (entity in this.entities) {
            entity.updateSprites(this);
        }
    }

    public function step(time: Float) {
        return this.space.step(time);
    }
}