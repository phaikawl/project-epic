package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.util.FlxColor;
import flixel.util.FlxPoint;
import flash.display.DisplayObject;
import nape.dynamics.InteractionFilter;
import nape.phys.Body;
import nape.phys.BodyType;
import nape.shape.Circle;
import nape.shape.Polygon;
import nape.util.ShapeDebug;
import nape.util.Debug;
import World.Entity;
import World.PlayerEntity;
import nape.constraint.AngleJoint;
import nape.constraint.Constraint;
import nape.constraint.DistanceJoint;
import nape.constraint.PivotJoint;
import nape.constraint.WeldJoint;
import nape.geom.Vec2;
import nape.callbacks.CbType;
import nape.callbacks.InteractionType;
import nape.callbacks.InteractionListener;
import nape.callbacks.CbEvent;
import nape.phys.Material;


class PlayState extends FlxState
{	
	var world: World;
	var player: PlayerEntity;
	var map: Entity;
	var debug: Debug;
	var wheels: Array<Body>;
	override public function create():Void
	{
		this.bgColor = 0xffffffff;

		FlxG.log.redirectTraces = false;
		var world = new World(new EwImporter("resources/world.ep",
            "resources/bodies.pbe"), 5, 1);
		var entities = world.entities;
		world.render(this, "resources/");
		var map = entities["map1"];
		// FlxG.camera.setBounds(0, 0, map.sprite.frameWidth, map.sprite.frameHeight);
		// FlxG.camera.antialiasing = true;
		// FlxG.camera.follow(player.sprite);
		// FlxG.worldBounds.set(0, 0, map.sprite.frameWidth, map.sprite.frameHeight);
		map.body.setShapeFilters(new InteractionFilter(2, ~0));
		map.body.setShapeMaterials(new Material(0.3,2,0.1,0.9,100));
		map.body.cbTypes.add(world.SURFACE);
		this.world = world;
		var player = cast(entities["player1"], PlayerEntity);
		player.body.setShapeFilters(new InteractionFilter(1, ~(0)));
		player.init(-12, 8, 4);
		player.setWheelFilters(new InteractionFilter(4, ~(4 | 1)));
		for (entity in entities) {
			if (entity.spec == "jumpRock") {
				entity.body.setShapeFilters(new InteractionFilter(8, ~(0)));
				entity.body.cbTypes.add(world.SURFACE);
				entity.body.setShapeMaterials(new Material(0.3,2,0.1,0.9,100));
			}	
		}

		this.map = map;
		this.player = player;
		var debug:Debug = new ShapeDebug(1000, 1000, this.bgColor);
		var display:DisplayObject = debug.display;
		FlxG.stage.addChild(display);
		this.debug = debug;
		 debug.drawConstraints=false;
	    //Scaling
	    // debug.display.x = 0;
	    // debug.display.y = 300;
	    debug.display.scaleY = 3;
	    debug.display.scaleX = 3;
	}
			
	
	override public function update():Void
	{
		super.update();
		world.step(0.1);
		world.updateSprites();
		var debug = this.debug;
				// Clear the debug display.
		debug.clear();
		// Draw our Space.
		debug.draw(world.space);
		// Flush draw calls, until this is called nothing will actually be displayed.
		debug.flush();

		// player.moveReset();

		if (FlxG.keyboard.pressed("S"))
		{
			player.move(-1);
		}

		if (FlxG.keyboard.pressed("F"))
		{
			player.move(1);
			// if (player.jumping) {
			// 	player.body.velocity.x = 2;
			// } else {
			// 	wheels[0].angularVel = aV;
			// 	wheels[1].angularVel = aV;
			// }
		}

		if (FlxG.keyboard.pressed("E"))
		{
			player.jump();
		}

		if (Math.abs(player.body.rotation) > Math.PI/2) {
			player.body.applyAngularImpulse(-player.body.rotation/2);
		}

		// var wheel = wheels[1];
		// wheel.angularVel = 0;
		// if (FlxG.keyboard.pressed("LEFT"))
		// {
		// 	wheel.angularVel = -1.5;
		// }

		// if (FlxG.keyboard.pressed("RIGHT"))
		// {
		// 	wheel.angularVel = 1.5;
		// }

		// if (FlxG.keyboard.pressed("UP"))
		// {
		// 	wheel.velocity.y = -6;
		// }
	}
}