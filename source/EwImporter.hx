import haxe.Json;
import nape.geom.Vec2; 
import nape.shape.Shape;
import nape.shape.Circle;
import nape.shape.Polygon;
import openfl.Assets;

class Coord {
    public static function transform(v: Vec2): Vec2 {
        return new Vec2(v.x, v.y);
    }

    public static function adjust(v: Vec2, scale: Float, width: Float, height: Float): Vec2 {
        return new Vec2(v.x*width*scale, (height-(v.y*width))*scale);
        // trace(v.x, v.y);
    }
}

interface BodyShape {
    function adjusted(scale: Float, width: Float, height: Float): BodyShape;
    function toPhysicsShape(): Shape;
}

typedef Point = {
    x: Float,
    y: Float
}

typedef BodySpec = {
    shapes: List<BodyShape>,
    imagePath: String,
    name: String
}

typedef BodyInstance = {
    bodyspec: String,
    pos: Point,
    width: Float,
    height: Float,
    id: String
}

class PolygonShape implements BodyShape {
    var vertices: Array<Vec2>;

    public function new(vertices: Array<Vec2>) {
        this.vertices = vertices;
    }

    public static function fromSpec(vertices: Array<Point>) {
        return new PolygonShape([for (vertex in vertices) Coord.transform(new Vec2(vertex.x, vertex.y))]);
    }

    public function adjusted(scale: Float, width: Float, height: Float): BodyShape {
        var a = new Array();
        for (vertex in this.vertices) {
            var v = Coord.adjust(vertex, scale, width, height);
            a.push(v);
        }
        return new PolygonShape(a);
    }

    public function toPhysicsShape(): Shape {
        return new Polygon(this.vertices);
    }
}

class CircleShape implements BodyShape {
    var center: Vec2;
    var radius: Float;

    public function new(radius: Float, center: Vec2) {
        this.center = center;
        this.radius = radius;
    }

    public static function fromSpec(spec: {cx: Float, cy: Float, r: Float}) {
        return new CircleShape(spec.r, Coord.transform(new Vec2(spec.cx, spec.cy)));
    }

    public function adjusted(scale: Float, width: Float, height: Float): BodyShape {
        return new CircleShape(radius*width*scale, Coord.adjust(this.center, scale, width, height));
    }

    public function toPhysicsShape(): Shape {
        return new Circle(this.radius, this.center);
    }
}

/*
* Epic world importer
*/
class EwImporter {
    private var bodySpecs: Map<String, BodySpec>;
    private var bodies: Map<String, BodyInstance>;

    public function new(ewFilePath: String, pbeFilePath: String) {
        this.bodySpecs = new Map();
        this.bodies = new Map();
        //ew = Epic World, pbe = Physics Body Editor

        //Import body shape specifications from Physics Body Editor file
        var pbeCont = Assets.getText(pbeFilePath);
        var pbeData: {rigidBodies: Array<{name: String, imagePath: String, polygons: Array<Dynamic>, circles: Array<Dynamic>}>}
            = Json.parse(pbeCont);

        var specs = pbeData.rigidBodies;
        for (spec in specs) {
            var shapes = new List<BodyShape>();
            var polygons = spec.polygons;
            // flash.Lib.trace(polygons);
            var circles = spec.circles;
            for (polygon in polygons) {
                shapes.add(PolygonShape.fromSpec(polygon));
            }
            for (circle in circles) {
                shapes.add(CircleShape.fromSpec(circle));
            }
    
            //return;
            this.bodySpecs.set(spec.name, {shapes: shapes, imagePath: spec.imagePath, name: spec.name});
        }

        //Import instances from Epic World file
        var ewCont = Assets.getText(ewFilePath);
        var ewData: {instances: Array<BodyInstance>} = Json.parse(ewCont);
        for (body in ewData.instances) {
            // trace(body);
            this.bodies.set(body.id, body);
        }
        // trace(this.bodies);
    }

    public function getBodySpecs(): Map<String, BodySpec> {
        return this.bodySpecs;
    }

    public function getBodies(): Map<String, BodyInstance> {
        return this.bodies;
    }
}